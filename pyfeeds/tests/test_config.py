#!/usr/bin/env python
#
# test_config.py -
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#


import                 os
import os.path  as     op
import textwrap as     tw
from   unittest import mock

import pyfeeds.main as main

from pyfeeds.common import tempdir


def test_loadPyfeedsConfig():

    cfg = tw.dedent("""
    [general]
    benchmarkDir = some/dir
    inputDir     = ${SOME}/dir
    testDir      = $SOME/dir1
                   ~/somedir2
    outputDir    = ~/somedir
   [evalRoutines]
    """).strip()

    with tempdir():
        with open('pyfeeds.cfg', 'wt') as f:
            f.write(cfg)

        with mock.patch.dict(os.environ, SOME='some'):
            cfg = main.loadPyfeedsConfig('pyfeeds.cfg')

        home = op.expanduser('~')

        assert cfg.benchmarkDir == 'some/dir'
        assert cfg.inputDir     == 'some/dir'
        assert cfg.testDir      == ['some/dir1', f'{home}/somedir2']
        assert cfg.outputDir    == f'{home}/somedir'
