#!/usr/bin/env python
#
# test_evaluate.py -
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#

import os
import os.path as op

import numpy as np
import nibabel as nib

from . import makepaths, maketest, makepyfeeds, CaptureStdout

from pyfeeds import testing, evaluate
from pyfeeds.common import tempdir


def test_evaluateTestAgainstBenchmark():
    with tempdir():
        maketest(  'tests/test1/feedsRun',   outputs=['file1', 'file2'])
        maketest(  'tests/test2/feedsRun.1', outputs=['file1', 'file2'])
        maketest(  'tests/test2/feedsRun.2', outputs=['file1', 'file2'])
        os.chmod(  'tests/test1/feedsRun'  , 0o755)
        os.chmod(  'tests/test2/feedsRun.1', 0o755)
        os.chmod(  'tests/test2/feedsRun.2', 0o755)
        os.mkdir('output/')
        os.mkdir('benchmarks/')

        makepaths(['benchmarks/test1/file1',
                   'benchmarks/test1/file2',
                   'benchmarks/test2/1/file1',
                   'benchmarks/test2/1/file2',
                   'benchmarks/test2/2/file1',
                   'benchmarks/test2/2/file2'])

        pyf = makepyfeeds(command='run',
                          testDir=['tests'],
                          outputDir='output',
                          benchmarkDir='benchmarks',
                          overwrite=True)

        for test in pyf.tests:
            assert testing.runTest(pyf,
                                   test,
                                   'output',
                                   None,
                                   'benchmarks',
                                   os.environ.copy(),
                                   False) == (True, 0)

        for resfile in ['output/test1/feedsResults.log',
                        'output/test2/1/feedsResults.log',
                        'output/test2/2/feedsResults.log']:
            assert op.exists(resfile)
            with open(resfile) as f:
                lines = f.readlines()
            lines = [l.strip() for l in lines]
            lines = [l for l in lines if l != '']
            assert len(lines) == 3
            for l in lines[1:]:
                assert 'PASS' in l


def test_evalVectorImage():

    vecarr1 = -1 + 2 * np.random.random((10, 10, 10, 3))
    vecarr2 = -1 + 2 * np.random.random((10, 10, 10, 3))

    with tempdir():

        pyf    = makepyfeeds()
        fname1 = 'image1.nii.gz'
        fname2 = 'image2.nii.gz'

        nib.Nifti1Image(vecarr1, np.eye(4)).to_filename(fname1)
        nib.Nifti1Image(vecarr2, np.eye(4)).to_filename(fname2)

        assert evaluate.evalVectorImage(fname1, fname1, pyf=pyf) == 0
        assert evaluate.evalVectorImage(fname2, fname2, pyf=pyf) == 0
        assert evaluate.evalVectorImage(fname1, fname2, pyf=pyf) != 0
        assert evaluate.evalVectorImage(fname1, fname1)          == 0
        assert evaluate.evalVectorImage(fname2, fname2)          == 0
        assert evaluate.evalVectorImage(fname1, fname2)          != 0


def test_evalImage():

    arr1 = -1 + 2 * np.random.random((10, 10, 10, 10))
    arr2 = -1 + 2 * np.random.random((10, 10, 10, 10))

    with tempdir():

        pyf    = makepyfeeds()
        fname1 = 'image1.nii.gz'
        fname2 = 'image2.nii.gz'

        nib.Nifti1Image(arr1, np.eye(4)).to_filename(fname1)
        nib.Nifti1Image(arr2, np.eye(4)).to_filename(fname2)

        assert evaluate.evalImage(fname1, fname1, pyf=pyf) == 0
        assert evaluate.evalImage(fname2, fname2, pyf=pyf) == 0
        assert evaluate.evalImage(fname1, fname2, pyf=pyf) != 0
        assert evaluate.evalImage(fname1, fname1)          == 0
        assert evaluate.evalImage(fname2, fname2)          == 0
        assert evaluate.evalImage(fname1, fname2)          != 0


def test_evalHeader():

    arr1 = -1 + 2 * np.random.random((10, 10, 10, 10))
    arr2 = -1 + 2 * np.random.random((10, 10, 10, 10))
    arr3 = -1 + 2 * np.random.random((10, 10, 10, 20))
    arr4 = -1 + 2 * np.random.random(( 5,  5,  5, 20))

    with tempdir():

        pyf    = makepyfeeds()
        fname1 = 'image1.nii.gz'
        fname2 = 'image2.nii.gz'
        fname3 = 'image3.nii.gz'
        fname4 = 'image4.nii.gz'

        nib.Nifti1Image(arr1, np.eye(4)).to_filename(fname1)
        nib.Nifti1Image(arr2, np.eye(4)).to_filename(fname2)
        nib.Nifti1Image(arr3, np.eye(4)).to_filename(fname3)
        nib.Nifti1Image(arr4, np.eye(4)).to_filename(fname4)

        assert evaluate.evalHeader(fname1, fname1, pyf=pyf) == 0
        assert evaluate.evalHeader(fname2, fname2, pyf=pyf) == 0
        assert evaluate.evalHeader(fname3, fname3, pyf=pyf) == 0
        assert evaluate.evalHeader(fname4, fname4, pyf=pyf) == 0
        assert evaluate.evalHeader(fname1, fname1)          == 0
        assert evaluate.evalHeader(fname2, fname2)          == 0
        assert evaluate.evalHeader(fname3, fname3)          == 0
        assert evaluate.evalHeader(fname4, fname4)          == 0

        assert evaluate.evalHeader(fname1, fname2)          == 0
        assert evaluate.evalHeader(fname1, fname3)          != 0
        assert evaluate.evalHeader(fname1, fname4)          != 0
        assert evaluate.evalHeader(fname1, fname3, ndims=3) == 0
        assert evaluate.evalHeader(fname3, fname4)          != 0
        assert evaluate.evalHeader(fname3, fname4, ndims=3) != 0

        assert evaluate.evalHeaderRestrictDims(fname1, fname2, pyf=pyf) == 0
        assert evaluate.evalHeaderRestrictDims(fname1, fname3, pyf=pyf) != 0
        assert evaluate.evalHeaderRestrictDims(fname1, fname2)          == 0
        assert evaluate.evalHeaderRestrictDims(fname1, fname3)          != 0


def test_evalImageMaxDiff():

    arr1 = np.zeros((10, 10, 10))
    arr2 = np.zeros((10, 10, 10))

    arr1[0, 0, 0] = 100

    with tempdir():

        pyf    = makepyfeeds()
        fname1 = 'image1.nii.gz'
        fname2 = 'image2.nii.gz'
        nib.Nifti1Image(arr1, np.eye(4)).to_filename(fname1)
        nib.Nifti1Image(arr2, np.eye(4)).to_filename(fname2)

        assert evaluate.evalImageMaxDiff(fname1, fname1, pyf=pyf) == 0
        assert evaluate.evalImageMaxDiff(fname1, fname1)          == 0
        assert evaluate.evalImageMaxDiff(fname1, fname2)          == 100

def test_evalNumericalText():

    arr1 = np.random.random(100)
    arr2 = np.random.random(100) * 4

    with tempdir():

        pyf    = makepyfeeds()
        fname1 = 'data1.txt'
        fname2 = 'data2.txt'
        np.savetxt(fname1, arr1)
        np.savetxt(fname2, arr2)

        assert evaluate.evalNumericalText(fname1, fname1, pyf=pyf) == 0
        assert evaluate.evalNumericalText(fname1, fname1)          == 0
        assert evaluate.evalNumericalText(fname1, fname2)          != 0

def test_evalMD5():

    arr1 = np.random.random(100)
    arr2 = np.random.random(100) * 4

    with tempdir():

        pyf    = makepyfeeds()
        fname1 = 'data1.txt'
        fname2 = 'data2.txt'
        np.savetxt(fname1, arr1)
        np.savetxt(fname2, arr2)

        assert evaluate.evalMD5(fname1, fname1, pyf=pyf) == 0
        assert evaluate.evalMD5(fname1, fname1)          == 0
        assert evaluate.evalMD5(fname1, fname2)          != 0
