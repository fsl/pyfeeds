#!/usr/bin/env python
#
# __init__.py - The pyfeeds package.
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#

__version__ = '0.14.1'
"""The pyfeeds version number. """


from .common import (
    fslbin,
    readfsf,
    writefsf,
)


from .evaluate import (
    evalHeader,
    evalHeaderRestrictDims,
    evalImage,
    evalImageMaxDiff,
    evalNumericalText,
    evalVectorImage,
    evalPolarCoordinateImageGroup,
    evalGiftiVertices,
    evalMD5,
    cmpArrays,
    cmpVectorArrays
)
