##############################################################
# This file contains configuration settings used by pyfeeds.
# It is set up to run the example tests provided with pyfeeds.
#
# Everything that can be specified on the pyfeeds command line
# can be specified in a pyfeeds configuration file.
#
# A pyfeeds configuration file contains three sections:
#
#   - general:      All settings apart from '--evalRoutines'
#                   and '--tolerances'
#
#   - evalRoutines: Evaluation routines to use for different
#                   file types. For each file to be tested,
#                   the routine that corresponds to the first
#                   pattern (a sh-style wildcard) in this list
#                   which matches thename of the file is used
#                   to evaluate the file.
#
#                   The list here is traversed from bottom to
#                   top, so more specific tests should appear
#                   towards the bottom, and more general
#                   "catch-all" tests towards the top.
#
#                   Files which do not match any pattern in
#                   this list are not evaluated.
#
#   - tolerances:   Error tolerances to use for different file
#                   types. All pyfeeds evaluation routines
#                   return a number which (approximately)
#                   corresponds to a normalised difference
#                   between 0 and 1. The tolerances listed
#                   here define the threshold above which a
#                   file should be considered to have failed
#                   the test.
#
##############################################################


[general]

jobs             = 8
defaultTolerance = 0.05

benchmarkDir     = exampleBenchmarkData
inputDir         = exampleInputData
testDir          = examples

selfEval =
    flirt_self_eval
    shared_data_self_eval

fileGroups =
    mean_ph1samples.*,mean_th1samples.*
    mean_ph2samples.*,mean_th2samples.*
    mean_ph3samples.*,mean_th3samples.*
    merged_ph1samples.*,merged_th1samples.*
    merged_ph2samples.*,merged_th2samples.*
    merged_ph3samples.*,merged_th3samples.*

exclude =
    *.log
    *.html

[evalRoutines]
*.nii.gz    = evalHeader,evalImage
*.nii       = evalHeader,evalImage
*.img       = evalHeader,evalImage
*.surf.gii  = evalGiftiVertices
*.mat       = evalNumericalText
*.txt       = evalNumericalText
*_V1.nii.gz = evalHeader,evalVectorImage
*_V2.nii.gz = evalHeader,evalVectorImage
*_V3.nii.gz = evalHeader,evalVectorImage


[tolerances]
*zstat*        = 0.05
*cluster_mask* = 0.025
