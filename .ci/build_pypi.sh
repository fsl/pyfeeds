#!/usr/bin/env bash

set -e

pip install wheel setuptools twine
python setup.py sdist
python setup.py bdist_wheel
twine check dist/*
