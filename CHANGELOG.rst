``pyfeeds`` changelog
=====================


0.14.1 (Friday 31st January 2025)
---------------------------------

* Fixed a few bugs introduced in the previous release.
* Adjusted the ``evalHeader`` routine - now it compares ``dim`` and ``pixdim``
  for all valid dimensions by default. The ``alldims`` parameter has been
  replaced with a ``ndims`` parameter, allowing the number of dimensions to
  be set (e.g. when you only want to compare the first three dimensions).


0.14.0 (Friday 31st January 2025)
---------------------------------

* Adjusted the ``pyfeeds.evaluate.eval*`` routines so that they can be called
  by external scripts.


0.13.1 (Saturday 21st December 2024)
------------------------------------


* Pyfeeds now prints platform information (including the host name), to make
  it easier to know what system a test run was executed on.


0.13.0 (Tuesday 17th December 2024)
-----------------------------------


* Pyfeeds now ignores any tests found within the input data, benchmark data,
  or output directories, in case any of those are contained within the
  specified test directories.
* New `evalImageMaxDiff` evaluation routine, which evaluates an image based
  on the maximum absolute difference to the benchmark image.


0.12.4 (Thursday 18th January 2024)
-----------------------------------


* Pyfeeds now sets a ``PYFEEDS_TESTING=1`` environment variable when calling
  `feedsRun` scripts.


0.12.3 (Friday 8th December 2023)
---------------------------------


* Separate out the core logic of ``evalVectorImage`` so it can be called
  programmatically more easily.


0.12.2 (Thursday 13th July 2023)
--------------------------------


* Pyfeeds will try to make ``feedsRun`` scripts executable if they are not
  already.


0.12.1 (Friday 31st March 2023)
-------------------------------


* Fix a bug in the ``ImageCache`` which broke support for non-NIfTI objects
  (e.g. ``nibabel.GiftiImage`` objects).


0.12.0 (Friday 31st March 2023)
-------------------------------


* Don't use `nibabel.Nifti1Image.get_fdata`, as it results in excessive memory
  usage, and the ``ImageCache`` calculates image sizes from the native data type
  of each image.
* Use 64 bit rather than 128 bit precision when evaluating image data.


0.11.1 (Thursday 2nd March 2023)
--------------------------------


* Update uses of the deprecated `nibabel.Nifti1Image.get_data` function.


0.11.0 (Tuesday 7th February 2023)
----------------------------------


* Environment variables and tildes (``~``) can now be used in the pyfeeds
  configuration file, for options which expect a file path.
* The pyfeeds hash file will now contain test names, purely for descriptive
  purposes.


0.10.1 (Monday 30th May 2022)
-----------------------------


* An ``--outputDir`` no longer needs to be specified - it defaults to
  ``pyfeeds-out``.


0.10.0 (Thursday 17th March 2022)
---------------------------------


* The ``--includeTests`` and ``--excludeTests`` options are now applied to
  full ``feedsRun`` script names - previously they were only applied to test
  directory names, meaning that individual ``feedsRun.<suffix>`` scripts could
  not be included/excluded.


0.9.10 (Wednesday 4th August 2021)
----------------------------------


* Work around for environments in which ``numpy`` doesn't support
  ``float128``.



0.9.9 (Wednesday 4th August 2021)
---------------------------------


* Fixed logging change from 0.9.8, as it accidentally caused logging from most
  ``pyfeeds`` modules to be suppresed.
* Adjusted the ``-V`` / ``--version`` option so that it is now passed as a
  top-level argument, i.e. ``pyfeeds -V``, instead of e.g. ``pyfeeds list
  -V``.


0.9.8 (Monday 2nd August 2021)
------------------------------


* Suppress log messages from third party libraries.


0.9.7 (Tuesday 23rd February 2021)
----------------------------------


* ``pyfeeds run`` and ``pyfeeds compare`` now return a non-zero exit code if
  any tests or file comparisons fail.


0.9.6 (Friday 11th December 2020)
---------------------------------


* Adminstrative release - ``pyfeeds`` will no longer be published to
  PyPI or conda-forge, but will be hosted internally at FMRIB.


0.9.5 (Wednesday 15th July 2020)
--------------------------------


* Fixed test benchmark directory search for tests with suffixes;
  i.e. a test with name `<mytest>/feedsRun.<suffix>` will save output
  to a directory called `<mytest>/<suffix>/`, but previously pyfeeds would
  search for benchmark data in a directory called `<mytest>`.


0.9.4 (Monday 25th November 2019)
---------------------------------


* Adjusted the ``evalVectorImage`` evaluation routine to skip the full test if
  the images are identical (as numerical imprecision can result in non-zero
  errors).



0.9.3 (Wednesday 16th October 2019)
-----------------------------------


* Fixed a bug where the current working directory would be searched for
  tests even if test directories were specified in a config file.


0.9.2 (Tuesday 15th October 2019)
---------------------------------


* Fixed a bug in the :meth:`.ImageCache.__setitem__` method, regarding
  in-memory images.


0.9.1 (Friday 4th October 2019)
-------------------------------


* Adjusted :func:`.cmpArrays` logic so that errors are scaled by the
  data range, rather than being scaled by individual voxel values.
* Adjusted the :class:`.ImageCache` so it ignores non-NIfTI images.
* Fixed initialisation so that test directories are not searched on the
  ``compare`` command.


0.9.0 (Monday 30th September 2019)
----------------------------------


* Test directories may now contain multiple ``feedsRun`` scripts - test
  scripts must have a name of the form ``feedsRun[.suffix]``.
* The output data for nested tests is now organised in the same way as the
  tests themselves. For example, the output data for a test called
  ``tests/subtests/test1/feedsRun`` will now be stored in
  ``outoput/subtests/test1/``, instead of ``output/subtests_test1/``.
* New ``--forceHashes`` option, to force calculation of test data hashes, even
  if the file modification times suggest that the data has not changed.
* Changed command line interface - there are no longer any "global" options
  (i.e. ``pyfeeds [global_options] command``). Instead, all options must
  come after the ``command``.
* The ``--inputDir`` and ``--benchmarkDir`` arguments to ``run``, ``bundle``,
  ``genhash`` and ``checkhash`` are now optional.
* If no test directories are specified on the command line, the current
  directory is searched for tests.
* Removed the ``--skipInputDir`` and ``--skipBenchmarkDir`` arguments to the
  ``bundle`` command.
* Changed the short form of the ``--cacheSize`` argument from ``-s`` to
  ``-cs``.
* Changed the ``compare`` command-line interface - the input and benchmark
  directories are now positional. If the ``--outputFile`` argument is not
  provided, the results are printed to standard output.


0.8.1 (Tuesday 24th September 2019)
-----------------------------------


* New ``--cacheSize`` option, allowing the size of the internal image cache to
  be limited.


0.8.0
-----


* Maintainence changes for publishing on PyPI/conda-forge.


0.7.1
-----


* Fix - the ``--includeTests`` and :attr:``--excludeTests`` options can now
  be specified in a configuration file.


0.7.0
-----


* New ``--includeTests`` and :attr:``--excludeTests`` options, allowing tests
  to be selected/deselected via glob-style wildcard patterns.


0.6.0
-----


* New ``--logFile`` option, allowing log messages to be directed to a file.
* File comparison results files are now sorted by the error value.
* New ability to define file groups, via the ``--fileGroups`` command line
  flag. This is intended for groups of files which need to be evaluated
  together.
* New :func:`evalPolarCoordinateImageGroup` evaluation routine, for evaluating
  polar coordinate images (e.g. as generated by ``bedpostx``)


0.5.0
-----


* Updated user documentation and examples.
* New `--overwrite` option to the `run` and `bundle` commands.
* Fixed bug in file name pattern matching logic.


0.4.1
-----


* Fixed a bug in the :func:`.createSandbox` function.



0.4.0
-----


* The ``compare`` command now accepts an ``--outputFile`` argument, rather
  than an ``--outputDir`` argument.
* Fixed bug where the ``--config`` option could not be used alongside the
  ``--verbose`` or ``--quiet`` options.


0.3.2
-----


* Fixed bug in :func:`.cmpArrays` function for images containing all zeros.


0.3.1
-----


* New ``evalGiftiVertices`` evaluation routine for comparing GIFTI surfaces.


0.3.0
-----


* New ``compare`` command, which can be used to compare two directories
  without having to run run any tests.


0.2.1
-----


* Small fixes to ``README.md``.


0.2.0
-----


* Adjusted the ``evalImage`` evaluation routine to be more robust with respect
  to extreme values, and to produce more "normalised" error values - the
  maximum possible error is now 2.0.


0.1.1
-----


* Fixed/refactored the ``evalVectorImage`` evaluation routine.


0.1.0
-----


* Added new ``evalHeaderRestrictDims`` evaluation routine.
* Added ``--version`` command line argument.


0.0.1
-----


* Initial working implementation.
